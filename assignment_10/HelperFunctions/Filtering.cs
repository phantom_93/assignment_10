﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace assignment_10
{
    #region Filter
    public enum Filter
    {
        ByAge,
        ByHeight,
        ByWeight,
        ByGender
    }
    #endregion

    #region Class Filtering
    public class Filtering
    {
        #region variable 
        private List<Animal> animals;
        #endregion 

        #region Property
        public bool ContinueFiltering { get; private set; }
        #endregion 

        #region Constructor
        // constructor that takes animal list parameter
        public Filtering(List<Animal> animals)
        {
            this.animals = animals;
        }
        #endregion

        #region UpdateList Method
        // update animal lists 
        public void UpdateList(List<Animal> animals)
        {
            this.animals = animals;
        }
        #endregion

        #region FilterSearch Method
        // this method enables the user to choose from options such as byage, bygender, byheight, byweight
        public void FilterSearch()
        {
            
            ContinueFiltering = true;
            GetInfo.PrintLine("\nAvailable Filters: ", ConsoleColor.DarkMagenta);

            string[] filterList = Enum.GetNames(typeof(Filter));
            foreach (string filter in filterList)
                GetInfo.PrintLine($" - {filter}", ConsoleColor.DarkMagenta);

            GetInfo.PrintLine("Please specify which filter you wish to use", ConsoleColor.DarkYellow);
            GetInfo.PrintLine("Type 'exit' to exit filtering mode.", ConsoleColor.DarkRed);

            Console.Write($"Filter By> ");
            string chosenFilter = Console.ReadLine();

            switch (chosenFilter.ToLower())
            {
                case "byage":
                    FilterAge();
                     break;
                case "bygender":
                    Console.WriteLine("Specify the gender(male, female) you want to see");
                    Console.Write($"Filter By> ");
                    string chosenGender = Console.ReadLine();
                    if (chosenGender == "male")
                    {
                        FilterMale(); 
                    }
                    else if(chosenGender == "female")
                    {
                        FilterFemale(); 
                    }
                    break;
                case "byheight":
                    FilterHeight(); 
                    break;
                case "byweight":
                    FilterWeight();
                    break;
                case "exit":
                    ContinueFiltering = false;
                    break;
                default:
                    GetInfo.PrintLine("Invalid filter entered", ConsoleColor.DarkRed);
                    break;
            }
        }
        #endregion 

        #region Filter Methods
        public void FilterHeight()
        {
            IEnumerable<Animal> filtered = from animal in this.animals
                                           where animal.Height > 10.0
                                           select animal;

            foreach (Animal a in filtered)
                a.Print();
        }


        public void FilterWeight()
        {
            IEnumerable<Animal> filtered = from animal in this.animals
                                           where animal.Weight > 150.0
                                           select animal;

            foreach (Animal a in filtered)
                a.Print();
        }


        public void FilterAge()
        {
            IEnumerable<Animal> filtered = from animal in this.animals
                                           where animal.Age > 3
                                           select animal;

            foreach (Animal a in filtered)
                a.Print();
        }

        public void FilterFemale()
        {
            IEnumerable<Animal> filtered = from animal in this.animals
                                           where animal.Gender == Gender.Female
                                           select animal;

            foreach (Animal a in filtered)
                a.Print();
        }

        public void FilterMale()
        {
            IEnumerable<Animal> filtered = from animal in this.animals
                                           where animal.Gender == Gender.Male
                                           select animal;

            foreach (Animal a in filtered)
                a.Print();
        }

        public delegate bool Check(Animal animal);

        public List<Animal> FilterInterface(Check check)
        {
            IEnumerable<Animal> filtered = from animal in animals
                                           where check(animal) == true
                                           select animal;

            return filtered.ToList();
        }
        #endregion 
    }
    #endregion 
}
