﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography;
using System.Transactions;
using assignment_10.Interfaces;

namespace assignment_10
{
    class GetInfo
    {
        #region Enum State 
        public enum State
        {
            viewExisting, 
            addNew
        }
        #endregion

        #region variables 
        static State currentState;
        static List<Animal> animals;
        static Filtering filter;
        #endregion 

        #region Function CreateExistingAnimals
        // this method is for creating existing animals
        static void CreateExistingAnimals()
        {
            // establish 4 tiger objects 
            WhiteBengalTiger sheerKhan = new WhiteBengalTiger("Sheer Khan", 5, 200, 12, Gender.Male, "white");
            SiberianTiger nala = new SiberianTiger("Nala", 6, 150, 10, Gender.Female, "bright orange");
            SumatranTiger ariel = new SumatranTiger("Ariel", 4, 180, 11, Gender.Female, "light orange");
            MalayanTiger catrina = new MalayanTiger("Catrina", 7, 140, 9, Gender.Female, "dark orange");



            //establish 4 Lions objects 

            WhiteLion simba = new WhiteLion("Simba", 6, 180, 7, Gender.Male, "white");
            KatangaLion sabrina = new KatangaLion("Sabrina", 5, 120, 7, Gender.Male, "reddish yellow");
            BarbaryLion nova = new BarbaryLion("Nova", 7, 130, 5, Gender.Male, "dark yellow");
            TransvaalLion aurora = new TransvaalLion("Aurora", 4, 140, 6.5, Gender.Male, "light yellow");


            // establish 4 wolves objects 
            GrayWolf destiny = new GrayWolf("Destiny", 5, 55, 6, Gender.Female, "gray");
            GrayWolf alpha = new GrayWolf("Alpha", 5, 60, 6, Gender.Male, "gray-white");
            GrayWolf apollo = new GrayWolf("Apollo", 7, 54, 6, Gender.Male, "lightgray");
            GrayWolf asher = new GrayWolf("Asher", 6, 52, 6, Gender.Male, "whitesmoke");


            // add animal objects to the animal list
            animals = new List<Animal>
            {
                sheerKhan,
                nala,
                ariel,
                catrina,
                simba,
                sabrina,
                nova,
                aurora,
                destiny,
                alpha,
                apollo,
                asher
            };
            // start a new filtering object for animals 
            filter = new Filtering(animals);
        }
        #endregion


        #region Function Welcome
        // this function enables the user to choose options from 
        public static bool Welcome()
        {
            bool runApp = true;

            CreateExistingAnimals();

            Console.WriteLine("Welcome to this program");
            Console.WriteLine("Here animals can be filtered by age, gender, height, weight");

            optionEntry:
            Console.WriteLine("Options: ");
            Console.WriteLine("- create: Create a new animal");
            Console.WriteLine("- filter: to filer all existing animals by age, gender, height or weight");
            Console.WriteLine("- list: View all existing animals");
            Console.WriteLine("- skills: View animals by their skills");
            Console.WriteLine("- exit: Exit the application");
            Console.WriteLine("Options: ");

            Console.Write("Please enter an option you wish to execute> ");
            string option = Console.ReadLine();

            switch(option)
            {
                case "create":
                    CreateNewAnimals(); 
                    break;
                case "filter":
                keepFilering:
                    filter.FilterSearch();
                    if (filter.ContinueFiltering)
                        goto keepFilering;
                    break;
                case "list":
                    ViewAnimals();
                    break;
                case "skills":
                    ViewSkills();
                    break;
                case "exit":
                    runApp = false;
                    break;
                default:
                    PrintLine("ERROR: Invalid option entered, please try again.", ConsoleColor.DarkRed);
                    goto optionEntry;
            }

            Console.WriteLine("");
            Proceed();

            return runApp;
        }
        #endregion


        #region Function CreateNewAnimals
        // this function enables the user to add new animals
        public static void CreateNewAnimals()
        {
        // Species
        reqAnimal:
            PrintLine("\nValid Animals: Lion, Tiger, Wolf", ConsoleColor.DarkYellow);
            Console.Write("Type) Please enter what type of animal this is: ");
            string animal = Console.ReadLine().ToLower();

            switch (animal)
            {
                case "lion":
                    break;
                case "tiger":
                    break;
                case "wolf":
                    break;
                default:
                    PrintLine($"ERROR: Invalid animal type entered (You entered \"{animal}\").", ConsoleColor.DarkRed);
                    goto reqAnimal;
            }

            // Name
            Console.Write("Name) Please enter the name of the animal: ");
            string name = Console.ReadLine();

            PrintLine($"Creating a {animal} named '{name}'", ConsoleColor.DarkGreen);

            // Random Items: Age, Gender, Height, Weight
            Animal animalObj = CreateAnimal(name, animal);
            animals.Add(animalObj);
            filter.UpdateList(animals);
        }
        #endregion

        #region CreateAnimal
        // this function add functionalties for the newly created animals
        public static Animal CreateAnimal(string name, string animal)
        {
            Random rnd = new Random();
            // age
            int age = rnd.Next(1, 14);

            if(animal == "tiger")
            {
                age = rnd.Next(1, 15);
            }
            if(animal== "wolf")
            {
                age = rnd.Next(1, 8);
            }
            // height
            double height = (rnd.NextDouble() + 1.0) * 20.0;
            // weight, gender
            double weight = (rnd.NextDouble() + 1.0) * 70.0;
            // gender
            Gender gender = default;
            string[] availableGenders = Enum.GetNames(typeof(Gender));
            gender = (Gender)rnd.Next(availableGenders.Length);

            // Create the animal
            Animal animalObj;

            switch(animal)
            {
                case "lion":
                    animalObj = new Lion(name, age, weight, height, gender);
                    break;
                case "tiger":
                    animalObj = new Tiger(name, age, weight, height, gender);
                    break;
                case "wolf":
                    animalObj = new Wolf(name, age, weight, height, gender);
                    break;
                default:
                    throw new Exception("ERROR OCCURED");
            }

            animalObj.Print();

            return animalObj;
        }
        #endregion

        #region Function ViewAnimals
        // this function is for listing animals
        public static void ViewAnimals()
        {
            foreach (Animal animal in animals)
            {
                animal.Print();
            }

        }
        #endregion

        #region Function ViewSkills
        // the following function is for viewing Skills of the existing animals
        public static void ViewSkills()
        {
            PrintLine("\nListing all hoping functions\n");
            PrintLine("----------------------");
            foreach (Animal animal in animals)
            {
                double dist = (new Random().NextDouble() + 1.0) * 60.0;
                animal.Hop(dist);
            }

            // View Swimmers
            PrintLine("\nListing all swimmers");
            PrintLine("----------------------");
            List<Animal> swimmers = filter.FilterInterface((animal) => {
                return animal is ISwimmer;
            });

            foreach (ISwimmer swimmer in swimmers)
                swimmer.Swim();



            PrintLine("\nListing all eaters");
            PrintLine("----------------------");
            List<Animal> eaters = filter.FilterInterface((animal) => {
                return animal is IEater;
            });

            foreach (IEater eater in eaters)
                eater.Eat(); 


            PrintLine("\nListing all yawners");
            PrintLine("----------------------");
            List<Animal> yawners = filter.FilterInterface((animal) => {
                return animal is IYawner;
            });

            foreach (IYawner yawner in swimmers)
                yawner.Yawn();


            PrintLine("\nListing all sleepers");
            PrintLine("----------------------");
            foreach (Animal animal in animals)
            {
                animal.Sleep(); 
            }



        }
        #endregion


        #region PrintLine Method
        public static void PrintLine(string line, ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(line);
            Console.ForegroundColor = ConsoleColor.White;
        }
        #endregion


        #region Proceed
        public static void Proceed()
        {
            PrintLine("Press any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }
        #endregion 
    }
}
