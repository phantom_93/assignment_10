﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_10
{
    public class SiberianTiger : Tiger
    {

        public SiberianTiger(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Striped = true;
            Skin = skin;
        }

    }
}
