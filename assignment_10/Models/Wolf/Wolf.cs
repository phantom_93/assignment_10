﻿using assignment_10.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_10
{
    #region Class Wolf
    public class Wolf : Animal, ISwimmer, IYawner, IEater
    {
        public string Skin { get; set; }
        public Wolf(string name, int age, double weight, double height, Gender gender) : base(name, age, weight, height, gender)
        {

        }

        public override void Hunt()
        {
            Console.WriteLine($"{Name} kills its prey by biting it in the neck area\n");
        }
        public override void Hop(double distanceM)
        {
            Console.WriteLine($"{Name} can hop {distanceM} m away\n");
        }


        public override void MakeNoise(string sound)
        {
            Console.WriteLine($"{Name} makes a sound: {sound}\n");
        }

        public override void Sleep()
        {
            Console.WriteLine($"{Name} has gone to bed. Zzzzzz\n");
        }

        public override void Print()
        {
            Console.WriteLine($"{Name} is a {Gender} and {Age} years old, and weigh {Weight}kg and {Height}feet long and it has the skin of {Skin}.\n");

        }
        public void Yawn()
        {
            Console.WriteLine($"{Name} is Yawning");
        }
        public void Swim()
        {
            Console.WriteLine($"{Name} loves swimming");
        }

        public void Eat()
        {

            Console.WriteLine($"{Name} eat the meat of the prey that it kills");
        }
    }
    #endregion
    
}
