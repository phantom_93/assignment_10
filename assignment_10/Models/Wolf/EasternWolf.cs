﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_10
{
    public class EasternWolf : Wolf
    {
        public EasternWolf(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }
    }
}
