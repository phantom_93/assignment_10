﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_10
{
    public class ArcticWolf : Wolf
    {
        public ArcticWolf(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }
    }
}
